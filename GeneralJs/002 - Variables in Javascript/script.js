var person = "John";
console.log(person);
person = "Nick";
console.log(person);

var firstName = "Bob";

let age = 30;
console.log(age);
age = 35;
console.log(age);

const city = "London";
console.log(city);

const color;
