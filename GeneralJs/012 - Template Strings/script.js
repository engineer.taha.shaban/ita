const name = 'John';
let age = 29;
let profession = 'instructor';

function personES5() {
	console.log(
		'My name is ' +
			name +
			' I am ' +
			age +
			' years old and I am an ' +
			profession
	);
}

personES5();

function personES6() {
	console.log(
		`My name is ${name} I am ${age} years old and I am an ${profession}`
	);
}

personES6();
