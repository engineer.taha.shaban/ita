const person = {
	myFunc() {
		console.log('Hi there');
	}
};

person.myFunc();
// person.firstname = 'John';
// person['lastname'] = 'Smith';
// person.firstname = 'Bob';
// person.son = {
// 	name: 'Nick'
// };
// person.son.age = 5;

// console.log(person);
// console.log(person.firstname);
