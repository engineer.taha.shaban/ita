const newEl = document.createElement("li");
// console.log(newEl);

const textData = document.createTextNode("News");
// console.log(textData);

newEl.appendChild(textData);
console.log(newEl);

const list = document.querySelector(".list");
list.appendChild(newEl);

list.insertBefore(newEl, list.children[2]);

list.removeChild(newEl);
