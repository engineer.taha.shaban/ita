function sum(a, b) {
  return a + b;
}

function sub(a, b) {
  return a - b;
}

module.exports = sum;

// module.exports.theSumFunctionOftwoNumbers = sum;
// module.exports.theSubFunctionOftwoNumbers = sub;
module.exports = {
  sum: sum,
  sub: sub,
};
